# Gurps 4th Edition

Includes:
- Draggable skills
- Draggable advantages/disadvantages
- Core skills and advantages (no descriptions)
- Roll skill or characteristic straight from sheet