const rootPath = 'systems/gurps-4/';

export function fullPath(rel) {
    return rootPath + rel;
}