import {fullPath} from "./paths.js";

function htmlFieldToKeyArray(name) {
    let keys = name.split("][");
    let first = keys.shift().split("[");

    return [...first, ...keys];
}

function deepSet(object, keys, value) {
    const key = keys.shift();
    if (keys.length === 0) {
        object[key] = value;
    } else {
        if (object[key] === undefined) {
            object[key] = {};
        }
        deepSet(object[key], keys, value);
    }
}

export function getItemsByType(object, type, sorted = true) {
    let items = object.items
        .filter((item) => item.type === type)
        .map((item) => {
            item.local_name = item.name;
            item._id = item.id;
            return item;
        });
    if (sorted) {
        return items.sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        })
    }
    return items;
}

export const bindings = {
    // button binding to dump out the current object in console
    debug($doc, object) {
        $doc.find('[data-action="debug"]').on('click', () => {
            console.log(object.data);
        });
    },
    // select all when inputs focused
    selectAllOnFocus($doc) {
        $doc.find('input').focus(ev => ev.currentTarget.select());
    },
    // bind item edit buttons
    // expected attributes:
    //      data-action="edit-item"
    //      data-item-id="{{ item._id }}"
    editButtons($doc, actor) {
        $doc.find('[data-action="edit-item"]').click(function() {
            const itemId = $(this).data('item-id');

            actor.getEmbeddedCollection("Item").get(itemId).sheet.render(true);
        });
    },
    deleteButtons($doc, actor) {
        const self = this;

        $doc.find('[data-action="delete-item"]').click(async function() {
            const itemId = $(this).data('item-id');
            const name = actor.getOwnedItem(itemId).data.name;
            const confirmed = await self.confirm(`Delete '${name}'?`, fullPath('templates/dialogs/confirm.html'), {
                name: name,
                action: 'delete',
            });

            if (confirmed) {
                actor.deleteOwnedItem(itemId);
            }
        });
    },
    popupForm(title, template, data = {}) {
        return new Promise((resolve, reject) => {
            $.get(template, async (content) => {
                let compiled = Handlebars.compile(content);
                await Dialog.prompt({
                    title: title,
                    content: compiled(data),
                    callback: async (dialog) => {
                        const fields = $(':input', dialog);
                        let data = {};
                        fields.each(function() {
                            const el = this;
                            if (!el.name.length) {
                                return;
                            }
                            const keys = htmlFieldToKeyArray(el.name);
                            deepSet(data, keys, el.value);
                        })
                        resolve(data);
                    }
                });
            });
        });
    },
    confirm(title, template, data = {}) {
        return new Promise((resolve, reject) => {
            $.get(template, async (content) => {
                let compiled = Handlebars.compile(content);
                await Dialog.confirm({
                    title: title,
                    content: compiled(data),
                    yes: async() => {
                        resolve(true);
                    },
                    no: async() => {
                        resolve(false);
                    }
                });
            });
        });
    },
    // (this is a hacky approach that appears to work)
    // expected attributes:
    //      data-bind="item"
    //      data-item-id="{{ item._id }}"
    //      data-key="{{ item property to set }}"
    inlineItemEdit($doc, actor) {
        // possibly just call `update` on the skill?
        $doc.find('[data-bind="item"]').on('change', function() {
            let $this = $(this);
            let item = $this.data('item-id');
            let key = $this.data('key');
            let value = $this.val();

            actor.items.get(item).data.data[key] = value;
            actor.getOwnedItem(item).data.data[key] = value;
        });

    }
};