import {parseDiceString} from "./dice.js";

const damageTable = [
    { thrust: 0, swing: 0 },
    { thrust: "1d-6", swing: "1d-5" }, // 1
    { thrust: "1d-6", swing: "1d-5" },
    { thrust: "1d-5", swing: "1d-4" },
    { thrust: "1d-5", swing: "1d-4" },
    { thrust: "1d-4", swing: "1d-3" },
    { thrust: "1d-4", swing: "1d-3" },
    { thrust: "1d-3", swing: "1d-2" },
    { thrust: "1d-3", swing: "1d-2" },
    { thrust: "1d-2", swing: "1d-1" },
    { thrust: "1d-2", swing: "1d" }, // 10
    { thrust: "1d-1", swing: "1d+1" },
    { thrust: "1d-1", swing: "1d+2" },
    { thrust: "1d", swing: "2d-1" },
    { thrust: "1d", swing: "2d" },
    { thrust: "1d+1", swing: "2d+1" },
    { thrust: "1d+1", swing: "2d+2" },
    { thrust: "1d+2", swing: "3d-1" },
    { thrust: "1d+2", swing: "3d" },
    { thrust: "2d-1", swing: "3d+1" },
    { thrust: "2d-1", swing: "3d+2" }, // 20
    { thrust: "2d", swing: "4d-1" },
    { thrust: "2d", swing: "4d" },
    { thrust: "2d+1", swing: "4d+1" },
    { thrust: "2d+1", swing: "4d+2" },
    { thrust: "2d+2", swing: "5d-1" },
    { thrust: "2d+2", swing: "5d-5" },
    { thrust: "3d-1", swing: "5d+1" },
    { thrust: "3d-1", swing: "5d+1" },
    { thrust: "3d", swing: "5d+2" },
    { thrust: "3d", swing: "5d+2" }, // 30
    { thrust: "3d+1", swing: "6d-1" },
    { thrust: "3d+1", swing: "6d-1" },
    { thrust: "3d+2", swing: "6d" },
    { thrust: "3d+2", swing: "6d" },
    { thrust: "4d-1", swing: "6d+1" },
    { thrust: "4d-1", swing: "6d+1" },
    { thrust: "4d", swing: "6d+2" },
    { thrust: "4d", swing: "6d+2" },
    { thrust: "4d+1", swing: "7d-1" },
    { thrust: "4d+1", swing: "7d-1" }, // 40
    { thrust: "4d+1", swing: "7d-1" },
    { thrust: "4d+1", swing: "7d-1" },
    { thrust: "4d+1", swing: "7d-1" },
    { thrust: "4d+1", swing: "7d-1" },
    { thrust: "5d", swing: "7d+1" },
    { thrust: "5d", swing: "7d+1" },
    { thrust: "5d", swing: "7d+1" },
    { thrust: "5d", swing: "7d+1" },
    { thrust: "5d", swing: "7d+1" },
    { thrust: "5d+2", swing: "7d+1" }, // 50
    { thrust: "5d+2", swing: "7d+1" },
    { thrust: "5d+2", swing: "7d+1" },
    { thrust: "5d+2", swing: "7d+1" },
    { thrust: "5d+2", swing: "7d+1" },
    { thrust: "6d", swing: "8d+1" },
    { thrust: "6d", swing: "8d+1" },
    { thrust: "6d", swing: "8d+1" },
    { thrust: "6d", swing: "8d+1" },
    { thrust: "6d", swing: "8d+1" },
    { thrust: "7d-1", swing: "9d" }, // 60
    { thrust: "7d-1", swing: "9d" },
    { thrust: "7d-1", swing: "9d" },
    { thrust: "7d-1", swing: "9d" },
    { thrust: "7d-1", swing: "9d" },
    { thrust: "7d+1", swing: "9d+2" },
    { thrust: "7d+1", swing: "9d+2" },
    { thrust: "7d+1", swing: "9d+2" },
    { thrust: "7d+1", swing: "9d+2" },
    { thrust: "7d+1", swing: "9d+2" },
    { thrust: "8d", swing: "10d" }, // 70
    { thrust: "8d", swing: "10d" },
    { thrust: "8d", swing: "10d" },
    { thrust: "8d", swing: "10d" },
    { thrust: "8d", swing: "10d" },
    { thrust: "8d+2", swing: "10d+2" },
    { thrust: "8d+2", swing: "10d+2" },
    { thrust: "8d+2", swing: "10d+2" },
    { thrust: "8d+2", swing: "10d+2" },
    { thrust: "8d+2", swing: "10d+2" },
    { thrust: "9d", swing: "11d" }, // 80
    { thrust: "9d", swing: "11d" },
    { thrust: "9d", swing: "11d" },
    { thrust: "9d", swing: "11d" },
    { thrust: "9d", swing: "11d" },
    { thrust: "9d+2", swing: "11d+2" },
    { thrust: "9d+2", swing: "11d+2" },
    { thrust: "9d+2", swing: "11d+2" },
    { thrust: "9d+2", swing: "11d+2" },
    { thrust: "9d+2", swing: "11d+2" },
    { thrust: "10d", swing: "12d" }, // 90
    { thrust: "10d", swing: "12d" },
    { thrust: "10d", swing: "12d" },
    { thrust: "10d", swing: "12d" },
    { thrust: "10d", swing: "12d" },
    { thrust: "10d+2", swing: "12d+2" },
    { thrust: "10d+2", swing: "12d+2" },
    { thrust: "10d+2", swing: "12d+2" },
    { thrust: "10d+2", swing: "12d+2" },
    { thrust: "10d+2", swing: "12d+2" },
    { thrust: "11d", swing: "13d" }, // 100
];

function calculateCharacteristicPoints(value, cost) {
    return (value - 10) * cost;
}

export function calculateAdvantagePoints(advantage) {
    let calc = advantage.data.points_calc;
    let level = advantage.data.value;

    if (!calc) {
        return 0;
    }

    if (calc.startsWith('-')) {
        level = level * -1;
        calc = calc.substr(1);
    }

    if (calc.startsWith('*')) {
        return level * parseInt(calc.substr(1));
    } else {
        let levels = calc.split(',');
        let cost = parseInt(levels[level - 1] || levels[levels.length - 1]);
        return cost;
    }
    return 0;
}

export function calculatePoints(actor) {
    let totals = {
        characteristics: calculateCharacteristicPoints(actor.data.ST, 10)
            + calculateCharacteristicPoints(actor.data.HT, 10)
            + calculateCharacteristicPoints(actor.data.DX, 20)
            + calculateCharacteristicPoints(actor.data.IQ, 20),
        secondary: 0,
        skills: 0,
        advantages: 0,
        total: 0
    };

    let extraSpeed = (actor.data.basic_speed - ((actor.data.HT + actor.data.DX) / 4));

    totals.secondary += (actor.data.HP.max - actor.data.ST) * 2; // HP
    totals.secondary += (actor.data.Will - actor.data.IQ) * 5; // Will
    totals.secondary += (actor.data.Per - actor.data.IQ) * 5; // Per
    totals.secondary += (actor.data.FP.max - actor.data.HT) * 3; // FP
    totals.secondary += (extraSpeed * 20); // basic_speed
    totals.secondary += (actor.data.basic_move - Math.floor(actor.data.basic_speed)) * 5; // basic_move

    actor.items.forEach((item) => {
        if (item.type === 'skill') {
            if (item.data.data.value === 1) {
                totals.skills += 1;
            } else if (item.data.data.value === 2) {
                totals.skills += 2;
            } else {
                totals.skills += parseInt((item.data.data.value) - 2) * 4;
            }
        } else if (item.type === 'advantage') {
            totals.advantages += calculateAdvantagePoints(item);
        }
    });
    totals.total = totals.characteristics + totals.skills + totals.advantages + totals.secondary;

    return totals;
}

export function calculateDamage(actor) {
    console.assert(damageTable.length === 101);
    if (actor.data.ST <= 0) {
        return damageTable[0];
    }
    if (actor.data.ST > 100) {
        let extra = Math.floor((actor.data.ST - 100) / 10);
        let thrust = extra + 11;
        let swing = extra + 13;

        return {
            thrust: `${thrust}d`,
            swing: `${swing}d`,
        };
    }
    return damageTable[actor.data.ST];
}

export function calculateBasicLift(actor) {
    let lift = (actor.data.ST * actor.data.ST) / 5;
    if (lift >= 10) {
        lift = Math.round(lift);
    }

    return lift;
}