import {bindings} from "./sheet-helpers.js";
import {fullPath} from "./paths.js";

function updateDiceModel(model, operator, part) {
    if (part.charAt(part.length - 1) === 'd') {
        part = parseInt(operator + part.substr(0, part.length - 1));
        model.dice += part;
    } else {
        part = parseInt(operator + part);
        model.mod += part;
    }

    return model;
}

export function parseDiceString(str) {
    let operator = '+';
    let result = {
        dice: 0,
        mod: 0,
    };

    for (const op of str.matchAll(/[+-]/g)) {
        let pos = str.indexOf(op);
        let part = str.substr(0, pos).trim();
        str = str.substr(pos + 1);
        result = updateDiceModel(result, operator, part);
        operator = op;
    }
    result = updateDiceModel(result, operator, str);

    return result;
}

async function doStatRoll($el, object, advanced = false) {
    // handle dice roller buttons on the character sheet
    let characteristic = $el.data('roll-characteristic');
    const text = $el.data('roll-text');
    const skill = $el.data('roll-skill');

    let target = 0;
    if (advanced) {
        let template = 'templates/dialogs/advanced-roll.html';
        if (skill) {
            template = 'templates/dialogs/advanced-skill-roll.html';
        }
        let advanced = await bindings.popupForm('Roll', fullPath(template), { characteristic: characteristic })

        if (skill) {
            characteristic = advanced.characteristic;
        }
        target += parseInt(advanced.modifier);
    }
    if (object.data.data[characteristic]) {
        target += parseInt(object.data.data[characteristic]);
    }

    const skillItem = object.getEmbeddedCollection("Item").get(skill);
    if (skillItem?.data.data.value) {
        console.log(skillItem);
        target += parseInt(skillItem.data.data.value);
        target += parseInt(skillItem.data.data.difficulty);
    }

    const roll = new Roll(`3d6`, object.getRollData());
    const result = await roll.roll();
    const total = result.dice[0].results.reduce((result, current) => result + current.result, 0);
    const effect = target - total;
    const colour = effect >= 0 ? 'green' : 'red';
    const description = `<h3>${text} (3d6 <= ${target})</h3>`;

    let effectStr;
    if (effect >= 0) {
        effectStr = `+${effect}`;
    } else {
        effectStr = `-${effect * -1 }`;
    }

    CONFIG.ChatMessage.documentClass.create({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({ actor: object }),
        type: CONST.CHAT_MESSAGE_TYPES.ROLL,
        flavor: `${description}<p style="color: ${colour};">${total}: effect ${effectStr}</p>`,
        sound: CONFIG.sounds.dice,
        roll: result
    });
}
async function doStringRoll($el, object, advanced = false) {
    const text = $el.data('roll-text');
    let rollStr = $el.data('roll-string');
    let advancedMessage = '';

    const dice = parseDiceString(rollStr);

    if (advanced) {
        let template = 'templates/dialogs/advanced-roll.html';
        let advanced = await bindings.popupForm('Roll', fullPath(template))

        advanced.modifier = parseInt(advanced.modifier);
        dice.mod += parseInt(advanced.modifier);

        if (advanced.modifier >= 0) {
            advancedMessage = ` (+${advanced.modifier})`;
        } else {
            advancedMessage = ` (${advanced.modifier})`;
        }
    }

    rollStr = `${dice.dice}d6`;
    if (dice.mod >= 0) {
        rollStr += `+${dice.mod}`;
    } else {
        rollStr += `${dice.mod}`;
    }

    const roll = new Roll(rollStr, object.getRollData());
    const result = roll.roll();
    const message = `${text}${advancedMessage} - ${rollStr}`;

    CONFIG.ChatMessage.entityClass.create({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({ actor: object }),
        type: CONST.CHAT_MESSAGE_TYPES.ROLL,
        flavor: message,
        sound: CONFIG.sounds.dice,
        roll: result
    });
}

async function doRoll($el, object, advanced = false) {
    if ($el.data('roll-string')) {
        doStringRoll($el, object, advanced);
    } else {
        doStatRoll($el, object, advanced);
    }
}


export function bindCharacterDiceRoller($doc, object) {
    $doc.find('.rollable').click(function(evt) {
        doRoll($(this), object, evt.shiftKey);
    });
}