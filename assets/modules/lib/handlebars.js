import {fullPath} from "./paths.js";

export function handlebarsHelpers() {
    Handlebars.registerHelper("slugify", function(value) {
        return value.slugify({ strict: true });
    });

    Handlebars.registerHelper("numberFormat", (value) => {
        if (value >= 0) {
            return "+" + value;
        }
        return value;
    });

    Handlebars.registerHelper("json", (value) => {
        return JSON.stringify(value);
    });

    Handlebars.registerHelper('select', function(value, options) {
        var $el = $('<select />').html(options.fn(this));
        $el.find('[value="' + value + '"]').attr({selected: 'selected'});

        return $el.html();
    });

    Handlebars.registerHelper('path', function(value) {
        return fullPath(value);
    });
    Handlebars.registerHelper('partial', function(value, opt) {
        return Handlebars.partials[fullPath(value)](opt.data.root);
    });

    Handlebars.registerHelper('skillMod', function(skill) {
        let modifier = parseInt(skill.data.data.value) + parseInt(skill.data.data.difficulty);
        if (modifier < 0) {
            return `${skill.data.data.characteristic} ${modifier}`;
        } else {
            return `${skill.data.data.characteristic} +${modifier}`;
        }
    });
}

export function loadHandlebarsPartials() {
    // specify partials that need loading here - otherwise they appear to be inaccessible
    loadTemplates([
        // 'systems/example-system/templates/...',
        fullPath('templates/dialogs/confirm.html'),
        fullPath('templates/partials/debug-entity.html'),

        fullPath('templates/actors/partials/characteristics.html'),
        fullPath('templates/actors/partials/skills.html'),
        fullPath('templates/actors/partials/advantages.html'),
        fullPath('templates/actors/partials/equipment.html'),

        fullPath('templates/items/partials/equipment-shared.html'),
    ]);
}