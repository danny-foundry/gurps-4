import {bindings} from "../lib/sheet-helpers.js";

export default class GurpsItemSheet extends ItemSheet {
    constructor(...args) {
        super(...args);

        // constructor extension
    }

    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            scrollY: [
                ".inventory .inventory-list", // todo - check what this actually does
            ],
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.object.limited; // permissions
        let type = this.object.type;

        return `systems/gurps-4/templates/items/${type}-sheet.html`;
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        return {
            item: this.object.data,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        const sheet = this;

        if (!this.isEditable) {
            return;
        }

        bindings.selectAllOnFocus($doc);
        bindings.debug($doc, this.object);

        super.activateListeners($doc);
    }

    // /* Validate values, enforce max/min etc */
    // /** @override */
    // _updateObject(evt, form) {
    //     return this.object.update(form);
    // }
}