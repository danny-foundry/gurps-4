import {bindings, getItemsByType} from "../lib/sheet-helpers.js";
import {bindCharacterDiceRoller} from "../lib/dice.js";
import {calculateAdvantagePoints} from "../lib/characters.js";

export default class GurpsActorSheet extends ActorSheet {
    constructor(...args) {
        super(...args);
        // constructor extension
    }

    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions

        return "systems/gurps-4/templates/actors/actor-sheet.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        return {
            actor: this.actor.data,
            skills: getItemsByType(this.actor.data, 'skill'),
            advantages: getItemsByType(this.actor.data, 'advantage').map((adv) => {
                adv.total_points = calculateAdvantagePoints(adv);
                return adv;
            }),
            gear: [
                ...getItemsByType(this.actor.data, 'melee-weapon'),
                ...getItemsByType(this.actor.data, 'ranged-weapon'),
                ...getItemsByType(this.actor.data, 'armour'),
                ...getItemsByType(this.actor.data, 'equipment'),
            ],
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        if (!this.isEditable) {
            return;
        }

        bindings.selectAllOnFocus($doc);
        bindings.editButtons($doc, this.actor);
        bindings.deleteButtons($doc, this.actor);
        bindings.inlineItemEdit($doc, this.actor);
        bindings.debug($doc, this.object);

        bindCharacterDiceRoller($doc, this.actor);

        super.activateListeners($doc);
    }

    /* Validate values, enforce max/min etc */
    /** @override */
    _updateObject(evt, form) {
        return this.object.update(form);
    }
}