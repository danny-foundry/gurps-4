export default class GurpsItem extends Item {
    // ensure the object matches the schema
    prepareBaseData() {
        if (this.data.type === 'skill') {
            this.data.data.levels = this.data.data.levels || 0;
        } else if (this.data.type === 'feat') {
            this.data.data.description = this.data.data.description || '';
        }
    }

    // set any computed attributes
    prepareDerivedData() {

    }
}