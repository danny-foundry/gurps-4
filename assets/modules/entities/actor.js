import {calculateBasicLift, calculateDamage, calculatePoints} from "../lib/characters.js";

export default class GurpsActor extends Actor {
    // ensure the object matches the schema
    prepareBaseData() {
        if (!this.data.data.HP) {
            this.data.data.HP = {};
        }
        if (!this.data.data.FP) {
            this.data.data.FP = {};
        }
        this.data.data.HP.value = this.data.data.HP.value || 10;
        this.data.data.HP.max = this.data.data.HP.max || 10;

        this.data.data.FP.value = this.data.data.FP.value || 10;
        this.data.data.FP.max = this.data.data.FP.max || 10;
    }

    // set any computed attributes
    prepareDerivedData() {
        this.data.data.basic_lift = calculateBasicLift(this.data);
        this.data.data.damage = calculateDamage(this.data);
        this.data.data.points = calculatePoints(this.data);
        this.data.data.dodge = Math.floor(this.data.data.basic_speed) + 3;
    }
}