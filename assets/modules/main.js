import GurpsActor from "./entities/actor.js";
import GurpsItem from "./entities/item.js";
import GurpsActorSheet from "./sheets/actor-sheet.js";
import GurpsItemSheet from "./sheets/item-sheet.js";
import {handlebarsHelpers, loadHandlebarsPartials} from "./lib/handlebars.js";
import {parseDiceString} from "./lib/dice.js";

/*
window.parseDiceString = parseDiceString;
let tests = [
    "1d+2",
    "3d-4",
    "1d+3d-2d+4",
    "1d+4-3",
];

tests.forEach((test) => {
    console.log(test);
    console.log(parseDiceString(test));
});
*/

Hooks.once('init', () => {
    // system initialisation

    // setup global scope
    game.gurps = {

    };

    // set config
    CONFIG.Actor.documentClass = GurpsActor;
    CONFIG.Item.documentClass = GurpsItem;
    CONFIG.time.roundTime = 1;
    CONFIG.Combat.initiative.formula = "1d6 + @attributes.DX.value";

    // unregister core sheets
    Actors.unregisterSheet("core", ActorSheet);
    Items.unregisterSheet("core", ItemSheet);

    // register custom sheets
    Actors.registerSheet("gurps", GurpsActorSheet, {
        types: ["gurps-character"],
        makeDefault: true,
        label: "Gurps.Character",
    });

    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["skill"],
        label: "Skill",
    });
    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["advantage"],
        label: "Advantage",
    });
    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["melee-weapon"],
        label: "Melee Weapon",
    });
    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["ranged-weapon"],
        label: "Ranged Weapon",
    });
    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["armour"],
        label: "Armour",
    });
    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["equipment"],
        label: "Equipment",
    });

    handlebarsHelpers();
    loadHandlebarsPartials();
});