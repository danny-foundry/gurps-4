<?php

function confirmContinue() {
    while(true) {
        echo "\e[32mContinue? [y/n]\e[0m";
        $answer = strtolower(trim(fgets(STDIN)));
        if ($answer === 'y' || $answer === 'yes') {
            return true;
        }
        if ($answer === 'n' || $answer === 'no') {
            return false;
        }
    }
}

function readMultiline($default) {
    $finished = false;
    $description = '';

    while(!$finished) {
        $line = trim(fgets(STDIN));
        if (!$line && !$description) {
            return $default;
        }

        if (substr($line, -1) === '\\') {
            $line = substr($line, 0, -1);
            $finished = false;
        } else {
            $finished = true;
        }
        $description .= $line."\n";
    }
    return trim($description);
}

class PackIterator {
    public $filter = '';
    public $multiline = false;
    public $typeFilter = '';
    public $onlyNull = false;

    public function getPacks() {
        $files = glob(dirname(__DIR__, 2).'/packs-src/*.db.json');
        if ($this->filter) {
            $files = array_filter($files, function($file) {
                return strpos(basename($file), $this->filter) !== false;
            });
        }
        return $files;
    }

    public function updatePack($file, Closure $read, Closure $write) {
        $data = json_decode(file_get_contents($file), true);
        if (!$data) {
            echo json_last_error_msg()."\n";
            exit;
        }

        unlink($file);
        $handle = fopen($file, 'w');
        fwrite($handle, '[');
        $index = 0;

        foreach ($data as $row) {
            if (feof(STDIN)) {
                break;
            }

            if (!$this->typeFilter || $row['type'] === $this->typeFilter) {
                $value = $read($row);

                if (!$this->onlyNull || !(bool)$value) {
                    echo "\e[35m{$row['name']}\e[0m\n";
                    echo "\e[36m{$value}\e[0m\n";

                    if ($this->multiline) {
                        $input = readMultiline($value);
                    } else {
                        $input = trim(fgets(STDIN)) ?? $value;
                    }

                    $row = $write($row, $input);
                }
            }

            if ($index !== 0){
                fwrite($handle, ",");
            }
            fwrite($handle, "\n\t");
            fwrite($handle, json_encode($row));
            $index ++;
        }

        fwrite($handle, "\n]");
        fclose($handle);
    }
}

class JournalIterator {
    public function getJournals()
    {
        return glob(dirname(__DIR__, 2).'/packs-journals/*.html');
    }
}