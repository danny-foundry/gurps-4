<?php

$dest = dirname(__DIR__).'/packs/';

function readNextLine($input) {
    $line = trim(fgets($input));

    while(!feof($input) && !$line) {
        $line = trim(fgets($input));
    }

    return $line;
}

$powers = glob(dirname(__DIR__).'/packs-powers/*.txt');
foreach ($powers as $power) {
    $name = substr(basename($power), 0, -4);
    $file = $dest.'power-'.$name.'.db';
    $handle = fopen($file, 'w');
    $input = fopen($power, 'r');

    $powerName = '';
    $powerPoints = '';
    $powerDescription = '';
    $count = 0;

    while(!feof($input)) {
        $powerName = readNextLine($input);
        $powerPoints = readNextLine($input);
        $powerDescription = readNextLine($input);

        if ($powerName && $powerPoints && $powerDescription) {
            $model = [
                '_id' => $name.'-'.++$count,
                'name' => $powerName,
                'type' => 'advantage',
                'data' => [
                    'type' => 'physical',
                    'category' => 'supernatural',
                    'description' => $powerDescription,
                    'value' => 1,
                    'points_calc' => $powerPoints,
                ],
                'flags' => [],
                'img' => 'icons/svg/mystery-man.svg',
                'permission' => ['default' => 0 ],
                'effects' => [],
            ];
            fwrite($handle, json_encode($model)."\n");
        }
    }
}

$packs = glob(dirname(__DIR__).'/packs-src/*.db.json');
$journals = glob(dirname(__DIR__).'/packs-journals/*/*.html');

foreach($packs as $pack) {
    $name = substr(basename($pack), 0, -5);
    $target = $dest.$name;
    if (file_exists($target)) {
        unlink($target);
    }

    $output = fopen($target, 'w');
    $data = json_decode(file_get_contents($pack));

    foreach ($data as $row) {
        fwrite($output, json_encode($row)."\n");
    }
    fclose($output);
}

$journalSets = [];
foreach($journals as $journal) {
    $set = basename(dirname($journal));
    if (!isset($journalSets[$set])) {
        $journalSets[$set] = [];
    }
    $journalSets[$set][] = $journal;
}

foreach ($journalSets as $set => $journals) {
    $file = $dest.$set.'.db';
    if (file_exists($file)) {
        unlink($file);
    }

    $handle = fopen($file, 'w');
    $sort = 10000;
    foreach ($journals as $journal) {
        // name, content, _id, sort
        $name = substr(basename($journal), 0, -5);
        $model = [
            'name' => strtr($name, ['-' => ' ']),
            '_id' => $name,
            'content' => file_get_contents($journal),
            'sort' => $sort++,
        ];
        fwrite($handle, json_encode($model)."\n");
    }
    fclose($handle);
}
