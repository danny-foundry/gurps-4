<?php

require __DIR__.'/helpers/iterator.php';

$opt = getopt('', [
    'filter:',
    'only-null',
]);
$filter = $opt['filter'] ?? '';

$iterator = new PackIterator();
$iterator->filter = $filter;
$iterator->multiline = true;
$iterator->onlyNull = isset($opt['only-null']);

$files = $iterator->getPacks($filter);

foreach ($files as $file) {
    echo "\e[33m{$file}\e[0m\n";
}
echo "\n\n";

foreach ($files as $file) {
    echo "\e[33m{$file}\e[0m\n";
    if (!confirmContinue()) {
        break;
    }

    $iterator->updatePack($file, fn($row) => $row['data']['description'] ?? '', function($row, $value) {
        $row['data']['description'] = $value;
        return $row;
    });
}