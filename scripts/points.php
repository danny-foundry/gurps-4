<?php
require __DIR__.'/helpers/iterator.php';

$opt = getopt('', [
    'filter:',
    'only-null',
]);
$filter = $opt['filter'] ?? '';

$iterator = new PackIterator();
$iterator->typeFilter = 'advantage';
$iterator->filter = $filter;
$iterator->onlyNull = $opt['only-null'] ?? false;

$files = getPacks();

foreach ($files as $file) {
    echo "\e[33m{$file}\e[0m\n";
}
echo "\n\n";

foreach ($files as $file) {
    echo "\e[33m{$file}\e[0m\n";
    if (!confirmContinue()) {
        break;
    }
    // fn($row, $val) => $row['data']['points_calc'] = $val
    $iterator->updatePack($file, fn($row) => $row['data']['points_calc'],function($row, $val) {
        $row['data']['points_calc'] = $val;
        return $row;
    });
}