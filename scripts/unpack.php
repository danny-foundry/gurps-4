<?php

$dest = dirname(__DIR__).'/packs-src/';
$packs = glob(dirname(__DIR__).'/packs/*.db');
foreach($packs as $pack) {
    $target = $dest.basename($pack).'.json';
    $backup = $target.'.bak';

    if (file_exists($target)) {
        if (file_exists($backup)) {
            unlink($backup);
        }
        file_put_contents($backup, file_get_contents($target));
        unlink($target);
    }

    $output = fopen($target, 'w');
    $input = fopen($pack, 'r');

    fwrite($output, '[');
    $lineNumber = 0;
    while($line = fgets($input)) {
        if (!trim($line)) {
            continue;
        }
        if ($lineNumber !== 0) {
            fwrite($output, ',');
        }
        fwrite($output, "\n\t".trim($line));
        $lineNumber ++;
    }
    fwrite($output, "\n]");

    fclose($input);
    fclose($output);
}